from setuptools import setup, find_packages

setup(
    name='mudkip',
    version='1.0.1',
    description='axolotl barebones only; all original axolotl stuff from @gucky92',
    author='Valentin Dalstein',
    author_email='vmd2133@columbia.edu',
    packages=find_packages(),
)
