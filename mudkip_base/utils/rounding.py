#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 16 17:00:39 2019

@author: matthias.christenson
"""

from numbers import Number

import numpy as np


around = np.vectorize(np.round)

def round(x, method=None, **kwargs):
    """
    Round a number or array-like object to the specified precision using a specified rounding method.

    Parameters:
        x (float or array-like): The number(s) to round.
        method (str or None): The rounding method to use (default: None).
            - If method is None, the default numpy rounding method is used.
            - If method is 'ceil', the values are rounded towards positive infinity.
            - If method is 'floor', the values are rounded towards negative infinity.
            - If method is 'trunc', the values are truncated towards zero.
            - If method is 'round', the values are rounded to the nearest integer.

    Returns:
        float or array-like: The rounded number(s) with the specified precision and rounding method.

    """
    return _round(x, method=method, **kwargs)


def _round(x, method=None, **kwargs):
    """
    Round a number or array-like object using a specific rounding method.

    Parameters:
        x (float or array-like): The number(s) to round.
        method (str or None): The rounding method to use (default: None).
            - If method is None, the default numpy rounding method is used.
            - If method is 'odd', the values are rounded to the nearest odd integer.
            - If method is 'even', the values are rounded to the nearest even integer.
            - If method is 'evendown', the values are rounded down to the nearest even integer.
            - If method is 'sig', 'sigdown', or 'sigup', the values are rounded to the specified number of significant digits.
            - If method is 'base', the values are rounded to the nearest multiple of a specified base.
            - If method is 'down', the values are rounded down to the specified number of decimal places.
            - If method is 'up', the values are rounded up to the specified number of decimal places.

    Returns:
        float or array-like: The rounded number(s) using the specified rounding method.

    """
    if not np.all(np.isfinite(x)):
        raise ValueError('Only float and int types for rounding')

    if method is None:
        return np.round(x, **kwargs)
    elif method == 'odd':
        return np.floor(x) + (x % 2 < 1)
    elif method == 'even':
        return np.floor(x) + (x % 2 >= 1)
    elif method == 'evendown':
        return np.floor(x-1) + ((x - 1) % 2 >= 1)
    elif method in ['sig', 'sigdown', 'sigup']:
        digits = kwargs.get('digits', 1)
        if isinstance(x, Number):
            if x == 0:
                return x
            else:
                decimals = int(digits_to_decimals(x, digits))
                if method == 'sig':
                    return np.round(x, decimals)
                elif method == 'sigdown':
                    return np.floor(x*10**decimals) / (10**decimals)
                elif method == 'sigup':
                    return np.ceil(x*10**decimals) / (10**decimals)
        else:
            x = np.nan_to_num(x)
            decimals = digits_to_decimals(x[x != 0], digits).astype(int)
            if method == 'sig':
                x[x != 0] = around(x[x != 0], decimals)
            elif method == 'sigdown':
                x[x != 0] = np.floor(x[x != 0]*10**decimals) / (10**decimals)
            elif method == 'sigup':
                x[x != 0] = np.ceil(x[x != 0]*10**decimals) / (10**decimals)
            return x
    elif method == 'base':
        base = kwargs.get('base', 5.)
        return np.round(x/base, 0) * base
    elif method == 'down':
        decimals = kwargs.get('decimals', 0)
        return np.floor(x*10**decimals) / (10**decimals)
    elif method == 'up':
        decimals = kwargs.get('decimals', 0)
        return np.ceil(x*10**decimals) / (10**decimals)
    else:
        raise NameError(f"rounding method {method} does not exist")
