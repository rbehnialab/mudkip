import os
import json
from pathlib import Path

# Mudkip config load
cwd = os.getcwd()

# Check if you are not within mudkip
if "site-packages" in os.path.dirname(__file__):
    FOLDER = cwd
else:  # you are within the package
    FOLDER = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))

# Load the config file
MUDKIP_CONFIG_FILE = os.path.join(FOLDER, "mudkip_config.json")
assert os.path.exists(
    MUDKIP_CONFIG_FILE), f"Please setup mudkip_config.json first!\nConfig expected address: {MUDKIP_CONFIG_FILE}"

with open(MUDKIP_CONFIG_FILE) as airtable_file:
    MUDKIP_CONFIG = json.load(airtable_file)

# Airtable variables
ACCESS_TOKEN = MUDKIP_CONFIG["read_and_write_token"]
BASE_ID = MUDKIP_CONFIG["base_id"]
RIG_NAME = MUDKIP_CONFIG["rig_name"]
REC_TABLE_ID = MUDKIP_CONFIG[RIG_NAME]["rec_table_id"]
FLY_GENOTYPE = MUDKIP_CONFIG["fly_genotype"]
STOCKS = MUDKIP_CONFIG["stocks"]
FLY_SUBJECT = MUDKIP_CONFIG["fly_subject"]
# Engram variables mostly pathes
DRIVE = [os.path.join(*x) if isinstance(x, list) else x
         for x in MUDKIP_CONFIG["drive_name"]]
NAUTILUS_FOLDER = MUDKIP_CONFIG["nautilus_folder"]
RAW_DATA_FOLDER = [os.path.join(*x) if isinstance(x, list) else x
                   for x in MUDKIP_CONFIG["raw_data_folder"]]
ENGRAM_DRIVE = ""
for path in DRIVE:
    path_to_test = os.path.join(path)
    if os.path.exists(os.path.join(path_to_test, NAUTILUS_FOLDER)):
        ENGRAM_DRIVE = path_to_test
assert not (ENGRAM_DRIVE == ""), "Engram not mounted, or engram drive path not in mudkip_config drive_name options"


FULL_PATH_NAUTILUS_FOLDER = os.path.join(ENGRAM_DRIVE, NAUTILUS_FOLDER)
REC_CONFIG_FOLDER = Path(os.path.join(FULL_PATH_NAUTILUS_FOLDER, RIG_NAME))

MUDKIP_FOLDER_ENGRAM = Path(os.path.join(ENGRAM_DRIVE, MUDKIP_CONFIG["mudkip_folder"]))
MUDKIP_PROCESS_CONFIGS = Path(os.path.join(MUDKIP_FOLDER_ENGRAM, "configs"))
RAW_READER_CONFIGS = MUDKIP_PROCESS_CONFIGS / MUDKIP_CONFIG["raw_film_reader_configs"]
MOTION_CORR_CONFIGS = MUDKIP_PROCESS_CONFIGS / MUDKIP_CONFIG["motion_correction_configs"]
EXTRACTION_CONFIGS = MUDKIP_PROCESS_CONFIGS / MUDKIP_CONFIG["extraction_configs"]
PSTHS_CONFIGS = MUDKIP_PROCESS_CONFIGS / MUDKIP_CONFIG["dreye_psths"]

