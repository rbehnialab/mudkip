import numpy as np
import matplotlib.pyplot as plt


def plot_movie_frames(rec_id, movie, clipping_val_min=0, clipping_val_max=500):
    frame_indexes = [int(movie.shape[0] * x) for x in np.arange(0,1,0.25)]
    frames = [movie[i] for i in frame_indexes] 

    # Titles for each subplot
    titles = ['First Frame', '1/4th Frame', 'Middle Frame', '3/4th Frame']

    # Plot frames using subplots
    fig, axes = plt.subplots(2, 2, figsize=(6, 4))
    axes = axes.flatten()
    for i, (frame, title) in enumerate(zip(frames, titles)):
        im = axes[i].imshow(frame, vmin=clipping_val_min, vmax=clipping_val_max)
        axes[i].set_title(title, fontsize=8)
        axes[i].axis('off')  # Hide axes
    plt.tight_layout()
    # Adjust layout so colorbar can fit
    plt.subplots_adjust(right=0.9)

    # Create a shared colorbar
    cbar_ax = fig.add_axes([0.92, 0.151, 0.03, 0.699])
    fig.colorbar(im, cax=cbar_ax)

    # Create a shared legend
    handles = [plt.Line2D([0], [0])]
    fig.legend(handles=handles, fontsize=4, loc=(1, 1))

    plt.suptitle(f"movie frame extraction: rec_id {rec_id}", fontsize=12)
    plt.show()