import pandas as pd

from mudkip_base.utils.mudkip_config import RIG_NAME
from mudkip_base.utils.processing import engram_data_to_df


def add_odor_config_to_events(df):
    odor_df = engram_data_to_df(df["recording_id"].tolist(), "odor_config", RIG_NAME)
    odor_df = odor_df.set_index(df.index)
    explosion_normalized = pd.json_normalize(odor_df["odor_config"], max_level=0).set_index(df.index)
    final_odor_df = pd.concat([odor_df.drop(columns=["odor_config"]), explosion_normalized], axis=1)
    df = pd.concat([df, final_odor_df], axis=1)
    for index, row in df.iterrows():
        odor_events_dict = row["odor_events"]
        odor_events_dict["_odor_during_event"] = {str(ind): (odor_events_dict["odor"][str(ind)] if not pd.isna(odor_events_dict["odor"][str(ind)]) else odor_events_dict["odor_background"][str(ind)]) for ind in range(len(odor_events_dict["odor"]))}
        vials_config = row["vials_config"]
        vials_config_names = {
            key: f"{v['molecule'][:3]}_{v['solvent'][:3]}_{v['concentration']}"
            for key, v in vials_config.items()
            if "channel_name" in v and "odor" in v["channel_name"]
        }
        odor_events_dict["odor_info"] = {str(ind): vials_config[odor_events_dict["_odor_during_event"][str(ind)]] for ind in range(len(odor_events_dict["odor"]))}
        odor_events_dict["odor_id"] = {str(ind): vials_config_names[odor_events_dict["_odor_during_event"][str(ind)]] for ind in range(len(odor_events_dict["odor"]))}
        df.at[index, "odor_events"] = odor_events_dict
    return df
