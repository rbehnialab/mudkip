import csv
import os
import importlib
import json
import numpy as np
import pandas as pd
from pathlib import Path

from datetime import datetime

from mudkip_base.utils.mudkip_config import (
    RAW_READER_CONFIGS,
    MOTION_CORR_CONFIGS,
    MUDKIP_FOLDER_ENGRAM,
    EXTRACTION_CONFIGS,
    PSTHS_CONFIGS,
    FULL_PATH_NAUTILUS_FOLDER,
    DRIVE,
    RAW_DATA_FOLDER,
    RIG_NAME
)

DUR_KEY = 'dur'
SYNCED_DUR_KEY = 'synced_dur'
PAUSE_KEY = 'pause'
SYNCED_DELAY_KEY = 'synced_delay'
DELAY_KEY = 'delay'


def engram_data_to_df(keys, filename, rig_name):
    nautilus_path = FULL_PATH_NAUTILUS_FOLDER
    path_to_record = os.path.join(nautilus_path, rig_name)
    records = []
    for key in keys:
        with open(os.path.join(path_to_record, f"rec{key}", f"{filename}.json"), "r") as json_file:
            record = json.load(json_file)
        records.append(record)
    return pd.DataFrame(records)


def generate_data_folders():
    data_folders = [f"{d}{os.path.sep}{folder}"
                    for d in DRIVE for folder in RAW_DATA_FOLDER
                    ]
    return data_folders


def show_populate_functions(folder):
    json_names = []
    json_data = []

    for filename in os.listdir(folder):
        if filename.endswith(".json"):
            json_names.append(filename)
            with open(os.path.join(folder, filename), "r") as file:
                data = json.load(file)
                json_data.append(data)

    df = pd.json_normalize(json_data, sep=".")
    df.insert(0, "name", [x[:-5] for x in json_names])

    return df

def insert_populate_settings(folder, name: str, settings: dict):
    """
    Insert new populate settings.

    Parameters
    ----------
    folder : PosixPath
        The folder to store your config file in. Should be one of the following:
        RAW_READER_CONFIGS,MOTION_CORR_CONFIGS, EXTRACTION_CONFIGS, PSTHS_CONFIGS 
    name : str
        The name of your settings configuration; this is what you will use to 
        reference your settings.
    settings: dict
        A dictionary containing the function you will be using, and parameters 
        for the function. The key of the dictionary should correspond to that
        function parameter, and the value should be that parameter's argument.
        Example format:
        dict = {
            "function": "zseries.get_prairieview_data_for_piezo",
            "global_params": {
                "datafolders": ["/Volumes/behnia-labshare/raw2p/2p_data"],
                "savepath": ["/Users/sharonsu/Columbia/AnalysisCode/raw"],
                "days_before": 4,
                "n_jobs": None,
                "add_absolute_trigger_time": True,
                "trigger_column": ["trigger", "lineScan"],
                "save_locally": True
            }
        }
    
    Returns
    -------
    Outputs the updated set of configurations via show_populate_functions for that folder.

    Raises
    ------
    KeyError:
        Raises a except when there is no function key specified.
    """
    try:
        settings["function"]
    except KeyError:
        raise KeyError('Please specify in your settings dict which function to use.')
    
    with open(os.path.join(folder, name + '.json'), 'w') as fp:
        json.dump(settings,fp, indent=4)

    return show_populate_functions(folder)

def is_valid(value):
    """Check if the value is invalid (empty, NaN, or empty string)."""
    if value is None:
        return False
    if isinstance(value, float) and np.isnan(value):
        return False
    if isinstance(value, str) and value.strip() == "":
        return False
    return True


def save_new_row(data_type, row):
    csv_file_path = MUDKIP_FOLDER_ENGRAM / RIG_NAME / f"{data_type}.csv"
    storage_path = MUDKIP_FOLDER_ENGRAM / RIG_NAME / data_type
    liste_files = [f for f in os.listdir(storage_path) if f.endswith('.hdf5')]
    if not liste_files:
        index_next_file = 0
    else:
        index_next_file = max([int(x.replace(".hdf5", ""))
                               for x in liste_files]) + 1
    filename = f'{index_next_file}.hdf5'
    new_file_path = storage_path / filename
    if isinstance(row, pd.Series):
        row = pd.DataFrame(row).T
    row.to_hdf(new_file_path, key='df', mode='w')
    if data_type == "raw_two_photon_data":
        new_row = {
            "recording_id": row["recording_id"].iloc[0],
            "raw_two_photon_data_populate_settings": row["raw_two_photon_data_populate_settings"].iloc[0],
            "name": filename
        }
    elif data_type == "motion_corrected_data":
        new_row = {
            "recording_id": row["recording_id"].iloc[0],
            "raw_two_photon_data_populate_settings": row["raw_two_photon_data_populate_settings"].iloc[0],
            "motion_corrected_data_populate_settings": row["motion_corrected_data_populate_settings"].iloc[0],
            "name": filename
        }
    elif data_type == "extracted_data":
        new_row = {
            "recording_id": row["recording_id"].iloc[0],
            "raw_two_photon_data_populate_settings": row["raw_two_photon_data_populate_settings"].iloc[0],
            "motion_corrected_data_populate_settings": row["motion_corrected_data_populate_settings"].iloc[0],
            "extracted_data_populate_settings": row["extracted_data_populate_settings"].iloc[0],
            "name": filename
        }
    # Append the new row to the CSV file
    # Only if all the fields are defined
    for key, value in new_row.items():
        assert is_valid(value), f"Invalid value found for key {key}: {value}"

    with open(csv_file_path, 'a', newline='') as file:
        writer = csv.DictWriter(file, fieldnames=new_row.keys())
        writer.writerow(new_row)


def already_exist(df, row_to_check):
    if df.empty:
        return df
    if isinstance(row_to_check, pd.Series):
        row_to_check = row_to_check.to_dict()
    # Extract relevant columns from the DataFrame
    columns_to_check = row_to_check.keys()
    subset_df = df[list(columns_to_check)]

    # Create a boolean mask to find the matching rows
    mask = (subset_df == pd.Series(row_to_check)).all(axis=1)

    # Get the matching rows
    matching_row = df[mask]
    return matching_row


def extracted_data_save(extracted_data_df):
    # Data folder
    csv_path = MUDKIP_FOLDER_ENGRAM / RIG_NAME / "extracted_data.csv"
    data_path = MUDKIP_FOLDER_ENGRAM / RIG_NAME / "extracted_data"
    existing_files = pd.read_csv(csv_path)
    # Iterate over requested movies
    for index, row in extracted_data_df.iterrows():
        row_exists = already_exist(existing_files, row[[
            "recording_id",
            "raw_two_photon_data_populate_settings",
            "motion_corrected_data_populate_settings",
            "extracted_data_populate_settings"
        ]])
        if row_exists.empty:
            df = pd.DataFrame(row).T
            save_new_row("extracted_data", df)
        else:  # row already exist
            if "hdroi" in row["extracted_data_populate_settings"]:
                if row["Roi"] is not None:
                    filename = row_exists["name"].iloc[0]
                    file_path = data_path / filename
                    # Append the new roi value
                    df = pd.DataFrame(row).T
                    df.to_hdf(file_path, key='df', mode='w')
                else:
                    old_row_df = pd.read_hdf(str(data_path / row_exists["name"].iloc[0]))
                    extracted_data_df.loc[index,"Roi"] = old_row_df["Roi"]


def read_recording(config_name, two_photon_recordings):

    # Function config
    config_path = RAW_READER_CONFIGS / f"{config_name}.json"
    with open(config_path, 'r') as config_file:
        config = json.load(config_file)
    global_params = config["global_params"]

    # Data folder
    csv_path = MUDKIP_FOLDER_ENGRAM  / RIG_NAME / "raw_two_photon_data.csv"
    data_path = MUDKIP_FOLDER_ENGRAM / RIG_NAME / "raw_two_photon_data"
    existing_files = pd.read_csv(csv_path)

    if "datafolders" not in global_params:
        global_params.update({"datafolders": generate_data_folders()})

    func_param = config["function"].split(".")
    module = importlib.import_module("mudkip_base.movie_reader")
    sub_module = getattr(module, func_param[0])
    get_function = getattr(sub_module, func_param[1])

    # Initialization
    df = pd.DataFrame(
        columns=["recording_id",
                 "raw_two_photon_data_populate_settings",
                 "rate", "timestamps", "movie",
                 "tiff_folder_location", "imaging_offset",
                 "trigger_timestamps", "trigger", "odor_trigger",
                 "field_of_view", "pmt_gain",
                 "scan_line_rate", "dimension",
                 "location", "laser_power",
                 "laser_wavelength", "dwell_time",
                 "microns_per_pixel", "metadata_collection"
                 ]
    )

    # Iterate over requested movies
    for index, row in two_photon_recordings.iterrows():

        # Row to create
        computed_row = {}
        computed_row["recording_id"] = row["recording_id"]
        computed_row["raw_two_photon_data_populate_settings"] = config_name
        row_exists = already_exist(existing_files, computed_row)

        # Check existence
        if not (row_exists.empty):
            # Load the hdf5 file into a DataFrame
            new_row_df = pd.read_hdf(str(data_path / row_exists["name"].iloc[0]))
            df = pd.concat([df, new_row_df], ignore_index=True)
        # Or compute
        else:
            params = {}
            params.update(global_params)
            params["recording_type"] = row["recording_type"]
            params["file_extension"] = row["recording_file_id"]
            params["date"] = datetime.fromisoformat(
                row["recording_time"].replace("Z", "+00:00")).date()

            computed_row.update(get_function(**params))
            computed_row["metadata_collection"].pop("pv_vin")
            computed_row_df = pd.DataFrame([computed_row])
            save_new_row("raw_two_photon_data", computed_row_df)
            df = pd.concat([df, computed_row_df], ignore_index=True)
    df.set_index("recording_id", inplace=True, drop=False)
    df.rename_axis('recording_id_index_col', inplace=True)
    return df


def motion_correction(config_name, raw_two_photon_data):

    # Function config
    config_path = MOTION_CORR_CONFIGS / f"{config_name}.json"
    with open(config_path, 'r') as config_file:
        config = json.load(config_file)
    global_params = config["global_params"]

    # Data folder
    csv_path = MUDKIP_FOLDER_ENGRAM / RIG_NAME / "motion_corrected_data.csv"
    data_path = MUDKIP_FOLDER_ENGRAM / RIG_NAME / "motion_corrected_data"
    existing_files = pd.read_csv(csv_path)

    func_param = config["function"].split(".")
    module = importlib.import_module("mudkip_base.motion_correction")
    sub_module = getattr(module, func_param[0])
    get_function = getattr(sub_module, func_param[1])

    # Initialization
    df = pd.DataFrame(
        columns=["recording_id",
                 "raw_two_photon_data_populate_settings",
                 "motion_corrected_data_populate_settings",
                 "movie", "timestamps", "rate"
                 ]
    )

    # Iterate over requested movies
    for index, row in raw_two_photon_data.iterrows():

        # Row to create
        computed_row = {}
        computed_row["recording_id"] = row["recording_id"]
        computed_row["raw_two_photon_data_populate_settings"] = row["raw_two_photon_data_populate_settings"]
        computed_row["motion_corrected_data_populate_settings"] = config_name
        row_exists = already_exist(existing_files, computed_row)

        # Check existence
        if not (row_exists.empty):
            # Load the hdf5 file into a DataFrame
            new_row_df = pd.read_hdf(str(data_path / row_exists["name"].iloc[0]))
            df = pd.concat([df, new_row_df], ignore_index=True)
        # Or compute
        else:
            params = {}
            params.update(global_params)
            params["rate"] = row["rate"]
            params["movie"] = row["movie"]
            params["timestamps"] = row["timestamps"]

            computed_row.update(get_function(**params))
            computed_row_df = pd.DataFrame([computed_row])
            save_new_row("motion_corrected_data", computed_row_df)
            df = pd.concat([df, computed_row_df], ignore_index=True)
    df.set_index("recording_id", inplace=True, drop=False)
    df.rename_axis('recording_id_index_col', inplace=True)
    return df


def extraction(config_name, motion_corrected_data, update_hdroi=False):

    # Function config
    config_path = EXTRACTION_CONFIGS / f"{config_name}.json"
    with open(config_path, 'r') as config_file:
        config = json.load(config_file)
    global_params = config["global_params"]

    # Data Folder
    csv_path = MUDKIP_FOLDER_ENGRAM / RIG_NAME / "extracted_data.csv"
    data_path = MUDKIP_FOLDER_ENGRAM / RIG_NAME / "extracted_data"
    existing_files = pd.read_csv(csv_path)

    func_param = config["function"].split(".")
    module = importlib.import_module("mudkip_base.extraction")
    sub_module = getattr(module, func_param[0])
    get_function = getattr(sub_module, func_param[1])

    # Initialization
    df = pd.DataFrame(
        columns=["recording_id",
                 "raw_two_photon_data_populate_settings",
                 "motion_corrected_data_populate_settings",
                 "extracted_data_populate_settings",
                 "Roi"
                 ]
    )

    # Iterate over requested movies
    if "hdroi" in config_name:
        for index, row in motion_corrected_data.iterrows():

            # Row to create
            computed_row = {}
            computed_row["recording_id"] = row["recording_id"]
            computed_row["raw_two_photon_data_populate_settings"] = row["raw_two_photon_data_populate_settings"]
            computed_row["motion_corrected_data_populate_settings"] = row["motion_corrected_data_populate_settings"]
            computed_row["extracted_data_populate_settings"] = config_name
            row_exists = already_exist(existing_files, computed_row)

            # Check existence
            if not (row_exists.empty):
                if update_hdroi:
                    # Compute normally                
                    computed_row["Roi"] = None
                    computed_row_df = pd.DataFrame([computed_row])
                    df = pd.concat([df, computed_row_df], ignore_index=True) 
                else:
                    #  Load tge hdf5 file into a DataFrame
                    new_row_df = pd.read_hdf(str(data_path / row_exists["name"].iloc[0]))
                    if isinstance(new_row_df, pd.Series):
                        new_row_df = pd.DataFrame(new_row_df).T
                    df = pd.concat([df, new_row_df], ignore_index=True)
            # Or compute
            else:
                computed_row["Roi"] = None
                computed_row_df = pd.DataFrame([computed_row])
                df = pd.concat([df, computed_row_df], ignore_index=True)

        df.set_index("recording_id", inplace=True, drop=False)
        df.rename_axis('recording_id_index_col', inplace=True)
        for index, row in df.iterrows():
            if row["Roi"] is None:
                params = {}
                params.update(global_params)
                params["roi_function"] = config_name
                params["recording_id"] = row["recording_id"]
                params["extracted_df"] = df
                params["rate"] = motion_corrected_data["rate"].loc[row["recording_id"]]
                params["movie"] = motion_corrected_data["movie"].loc[row["recording_id"]]
                params["timestamps"] = motion_corrected_data["timestamps"].loc[row["recording_id"]]
                get_function(**params)
        return df

    else:

        for index, row in motion_corrected_data.iterrows():

            # Row to create
            computed_row = {}
            computed_row["recording_id"] = row["recording_id"]
            computed_row["raw_two_photon_data_populate_settings"] = row["raw_two_photon_data_populate_settings"]
            computed_row["motion_corrected_data_populate_settings"] = row["motion_corrected_data_populate_settings"]
            computed_row["extracted_data_populate_settings"] = config_name
            row_exists = already_exist(existing_files, computed_row)

            # Check existence
            if not (row_exists.empty):
                # Load the hdf5 file into a DataFrame
                new_row_df = pd.read_hdf(str(data_path / row_exists["name"].iloc[0]))
                if isinstance(new_row_df, pd.Series):
                    new_row_df = pd.DataFrame(new_row_df).T
                df = pd.concat([df, new_row_df], ignore_index=True)
            else:
                params = {}
                params.update(global_params)
                params["rate"] = row["rate"]
                params["movie"] = row["movie"]
                params["timestamps"] = row["timestamps"]

                computed_row.update(get_function(**params))
                computed_row_df = pd.DataFrame([computed_row])
                df = pd.concat([df, computed_row_df], ignore_index=True)
        df.set_index("recording_id", inplace=True, drop=False)
        df.rename_axis('recording_id_index_col', inplace=True)
        return df


def dreye_psths(config_name, events_df):
    config_path = PSTHS_CONFIGS / f"{config_name}.json"
    with open(config_path, 'r') as config_file:
        config = json.load(config_file)
    global_params = config["global_params"]

    func_param = config["function"].split(".")
    module = importlib.import_module("mudkip_base.traces")
    sub_module = getattr(module, func_param[0])
    get_function = getattr(sub_module, func_param[1])

    computed_rows = []
    for index, row in events_df.iterrows():
        params = {}
        params.update(global_params)
        params["signal"] = row["signal"]
        params["events"] = row["events"]
        params["timestamps"] = row["timestamps"]

        computed_row = {}
        computed_row["recording_id"] = row["recording_id"]
        computed_row["raw_two_photon_data_populate_settings"] = row["raw_two_photon_data_populate_settings"]
        computed_row["motion_corrected_data_populate_settings"] = row["motion_corrected_data_populate_settings"]
        computed_row["extracted_data_populate_settings"] = row["extracted_data_populate_settings"]
        computed_row["dreye_psths_populate_settings"] = config_name
        computed_row["cell_id"] = row["cell_id"]
        # computed_row["led_mixer_name"] = row["led_mixer_name"]
        computed_row["stim_config_name"] = row["stim_config_name"]

        computed_row["psths"] = get_function(**params)

        computed_rows.append(computed_row)
    df = pd.DataFrame(computed_rows)
    df.set_index("recording_id", inplace=True, drop=False)
    df.rename_axis('recording_id_index_col', inplace=True)
    return df
