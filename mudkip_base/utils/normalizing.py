#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Normalizing functions

Created on Fri Aug 16 17:11:13 2019

@author: matthias.christenson
"""

import numpy as np


def scale(arr, axis=None, has_nan=False, bounds=None):
    """
    Scale the array to be between 0 and 1.

    Args:
        arr: Input array.
        axis: The axis or axes along which to perform the scaling. If None, scaling is applied to the whole array.
        has_nan: Flag indicating if the array contains NaN values.
        bounds: Optional bounds to scale the array to, specified as a float or a tuple of (min, max).

    Returns:
        Scaled array.

    Raises:
        TypeError: If the input array has incompatible type.
    """

    try:
        if has_nan:
            minimum = np.nanmin(arr, axis=axis, keepdims=True)
            maximum = np.nanmax(arr, axis=axis, keepdims=True)
        else:
            minimum = np.min(arr, axis=axis, keepdims=True)
            maximum = np.max(arr, axis=axis, keepdims=True)
    except TypeError:
        if has_nan:
            minimum = np.nanmin(arr, axis=axis)
            maximum = np.nanmax(arr, axis=axis)
        else:
            minimum = np.min(arr, axis=axis)
            maximum = np.max(arr, axis=axis)

    # scale array
    scaled_arr = (arr - minimum)/(maximum - minimum)
    if bounds is None:
        return scaled_arr
    elif isinstance(bounds, (float, int)):
        return scaled_arr * bounds
    else:
        return scaled_arr * (bounds[1] - bounds[0]) + bounds[0]
