import requests
import time
import pandas as pd


PROXY_URL = "http://gw-srv-01.rc.zi.columbia.edu:3128"
API_PAGE_SIZE = 100
API_WAIT_TIME = 1.0 / 5.0


def get_entire_table(access_token, base_id, table_id):
    headers = {'Authorization': 'Bearer %s' % access_token}
    params = {}
    params.update({"pageSize": API_PAGE_SIZE})
    flag = True
    record_list = []
    while (flag):
        response = requests.request(
            "GET",
            f'https://api.airtable.com/v0/{base_id}/{table_id}',
            params=params,
            headers=headers,
            proxies={'https': PROXY_URL}
        )
        if response.status_code == 200:
            response_json = response.json()
            if "offset" not in response_json:
                record_list += [r["fields"] for r in response_json["records"]]
                flag = False
            else:
                params.update({"offset": response_json["offset"]})
                record_list += [r["fields"] for r in response_json["records"]]
        else:
            try:
                response.raise_for_status()
            except requests.exceptions.HTTPError as errh:
                print("HTTP Error")
                print(errh.args[0])
            print(response.status_code)
            print("https://airtable.com/developers/web/api/errors for more information")

        time.sleep(API_WAIT_TIME)
    return pd.DataFrame(record_list)
