#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
data statistics

Created on Fri Aug 16 17:44:13 2019

@author: matthias.christenson
"""

import numpy as np


def nonzero_mean(arr, axis=None, has_nan=False):
    """
    Calculate the nonzero mean of an array.

    Parameters:
        arr (ndarray): Input array.
        axis (int or tuple, optional): Axis or axes along which the mean is computed. Default is None.
        has_nan (bool, optional): Flag indicating whether the array contains NaN values. Default is False.

    Returns:
        ndarray: Mean values along the specified axis, ignoring zeros.

    """

    if has_nan:
        n = np.nansum(arr != 0, axis=axis)
        arr_sum = np.nansum(arr, axis=axis)
    else:
        n = np.sum(arr != 0, axis=axis)
        arr_sum = np.sum(arr, axis=axis)
    return np.true_divide(arr_sum, n)
