import pandas as pd
import numpy as np


def label_odor_and_leds_events(df):
    # real copy of df
    df_copy = df.copy(deep=True)
    for index, row in df_copy.iterrows():
        odor_events_dict = row["odor_events"]
        led_events_dict = row["led_events"]
        odor_events_df = pd.DataFrame(odor_events_dict)
        led_events_df = pd.DataFrame(led_events_dict)
        # add led or odor in front of each led events columns
        odor_events_df_renamed_cols = odor_events_df.copy().add_prefix('odor.')
        odor_events_cols = odor_events_df_renamed_cols.columns.to_list()
        led_events_df_renamed_cols = led_events_df.copy().add_prefix('led.')
        led_events_cols = led_events_df_renamed_cols.columns.to_list()

        for odor_index, odor_row in odor_events_df.iterrows():
            if pd.isna(odor_row.get("odor_background", np.nan)):
                odor_tsts = [odor_row["synced_delay"], odor_row["synced_delay"] + odor_row["dur"]]
                for led_index, led_row in led_events_df_renamed_cols.iterrows():
                    led_tsts = [led_row["led.synced_delay"], led_row["led.synced_delay"] + led_row["led.dur"]]
                    if (led_tsts[0] <= odor_tsts[0] <= led_tsts[1]) or (led_tsts[0] <= odor_tsts[1] <= led_tsts[1]):
                        odor_events_df.at[odor_index, led_events_cols] = led_row
                        break

        for led_index, led_row in led_events_df.iterrows():
            if pd.isna(led_row.get("led_background", np.nan)):
                led_tsts = [led_row["synced_delay"], led_row["synced_delay"] + led_row["dur"]]
                for odor_index, odor_row in odor_events_df_renamed_cols.iterrows():
                    odor_tsts = [odor_row["odor.synced_delay"], odor_row["odor.synced_delay"] + odor_row["odor.dur"]]
                    if (odor_tsts[0] <= led_tsts[0] <= odor_tsts[1]) or (odor_tsts[0] <= led_tsts[1] <= odor_tsts[1]):
                        led_events_df.at[led_index, odor_events_cols] = odor_row
                        break

        df_copy.at[index, "odor_events"] = odor_events_df
        df_copy.at[index, "led_events"] = led_events_df
    return df_copy
