import ipywidgets as widgets
import matplotlib.pyplot as plt
import numpy as np
from IPython.display import display
from IPython.display import clear_output
from mpl_interactions import image_segmenter
from scipy.ndimage.measurements import center_of_mass as com

from mudkip_base.extraction.signal_extract import \
    extract_rolling_dfof

def hdroi_segmenter(
    image,
    rec_id,
    extracted_df,
    movie,
    rate,
    timestamps,
    bg_subtract,
    nonzero,
    filter_type,
    filter_before,
    filter_kws,
    dfof_kws,
    n_roi_classes=20
):
    """
    Allows to interactively draw multiple regions of interests
    Parameters
    ---------
    image: matplotlib image to select ROIs over

    Returns
    --------
    roi_names: int
        names of ROIs
    masks : numpy float64.array
        binary masks,  each row corresponds to a different ROI and each column corresponds to a pixel in the image.
        The value of each element is 1 if the pixel is inside the ROI, else 0.
    """

    class_selector = widgets.Dropdown(options=list(range(1, n_roi_classes + 1)), description="Class", layout=widgets.Layout(width='200px'))
    erasing_button = widgets.Checkbox(value=False, description="Erasing", layout=widgets.Layout(width='200px'))
    submit_button = widgets.Button(description="Submit", layout=widgets.Layout(width='200px'))

    # Function to handle button click event
    def on_button_clicked(b):
        plt.close()
        submit_button.disabled = True
        clear_output(wait=True)

        names = []
        masks = []
        if np.max(multi_class_segmenter.mask) > 0.0:
            for ind in range(1, n_roi_classes+1):
                if float(ind) in multi_class_segmenter.mask:
                    names.append(len(names))
                    masks.append(multi_class_segmenter.mask==float(ind))
            masks = np.array(masks)
            if len(masks):  # skip if no rois exist
                calc_traces, calc_timestamps, calc_rate = extract_rolling_dfof(
                    movie, masks,
                    timestamps=timestamps,
                    rate=rate,
                    bg_subtract=bg_subtract,
                    nonzero=nonzero,
                    filter_type=filter_type,
                    filter_before=filter_before,
                    filter_kws=filter_kws,
                    dfof_kws=dfof_kws
                )

            container = []

            for cell_id, mask in zip(names, masks):
                metadata = {
                    'com': com(mask),
                    'pixels': np.sum(mask)
                }
                label = f"handrawn_cell{cell_id}"

                container.append(dict(
                    metadata=metadata,
                    label=label,
                    cell_id=cell_id,
                    mask=mask,
                    rate=calc_rate,
                    timestamps=calc_timestamps,
                    signal=calc_traces[:, cell_id]
                ))
            extracted_df["Roi"].loc[rec_id] = container

    submit_button.on_click(on_button_clicked)

    def update(change):
        multi_class_segmenter.current_class = class_selector.value
        multi_class_segmenter.erasing = erasing_button.value

    erasing_button.observe(update, names="value")
    class_selector.observe(update, names="value")

    multi_class_segmenter = image_segmenter(
        image,
        nclasses=n_roi_classes,
        mask_alpha=0.76,
        figsize=(6,6)
        )

    display(widgets.HBox([class_selector, erasing_button, submit_button]))
    display(multi_class_segmenter)
